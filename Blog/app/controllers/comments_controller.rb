class CommentsController < ApplicationController
  before_filter :authenticate_user!, except: [:show, :index, :create]
  def create
	@post = Post.find(params[:post_id])
	@comment = @post.comments.build(params.require(:comment).permit(:text))
	@comment.save
	redirect_to @post
  end
	
  def destroy
     @comment = Comment.find(params[:id])
     @comment.destroy
     redirect_to @comment.post
  end
end
